package com.zeitheron.thaumicadditions.init;

import com.zeitheron.thaumicadditions.items.ItemDNASample;
import com.zeitheron.thaumicadditions.items.ItemKnowledgeTome;
import com.zeitheron.thaumicadditions.items.ItemMaterial;
import com.zeitheron.thaumicadditions.items.ItemSaltEssence;
import com.zeitheron.thaumicadditions.items.ItemSealSymbol;
import com.zeitheron.thaumicadditions.items.ItemZeithScale;
import com.zeitheron.thaumicadditions.items.baubles.ItemFragnantPendant;
import com.zeitheron.thaumicadditions.items.baubles.ItemRechargeCharm;

public class ItemsTAR
{
	public static final ItemMaterial MITHRILLIUM_NUGGET = new ItemMaterial("mithrillium_nugget", "nuggetMithrillium");
	public static final ItemMaterial ADAMINITE_NUGGET = new ItemMaterial("adaminite_nugget", "nuggetAdaminite");
	public static final ItemMaterial MITHRILLIUM_INGOT = new ItemMaterial("mithrillium_ingot", "ingotMithrillium");
	public static final ItemMaterial ADAMINITE_INGOT = new ItemMaterial("adaminite_ingot", "ingotAdaminite");
	public static final ItemMaterial MITHMINITE_INGOT = new ItemMaterial("mithminite_ingot", "ingotMithminite");
	public static final ItemMaterial MITHRILLIUM_PLATE = new ItemMaterial("mithrillium_plate", "plateMithrillium");
	public static final ItemMaterial ADAMINITE_PLATE = new ItemMaterial("adaminite_plate", "plateAdaminite");
	public static final ItemMaterial MITHMINITE_PLATE = new ItemMaterial("mithminite_plate", "plateMithminite");
	public static final ItemMaterial SEAL_GLOBE = new ItemMaterial("seal_globe");
	public static final ItemMaterial ODOUR_POWDER = new ItemMaterial("odour_powder");
	public static final ItemMaterial ZEITH_SCALES = new ItemZeithScale();
	public static final ItemSaltEssence SALT_ESSENCE = new ItemSaltEssence();
	public static final ItemDNASample ENTITY_CELL = new ItemDNASample();
	public static final ItemRechargeCharm RECHARGE_CHARM = new ItemRechargeCharm();
	public static final ItemSealSymbol SEAL_SYMBOL = new ItemSealSymbol();
	public static final ItemKnowledgeTome KNOWLEDGE_TOME = new ItemKnowledgeTome();
	public static final ItemFragnantPendant FRAGNANT_PENDANT = new ItemFragnantPendant();
}